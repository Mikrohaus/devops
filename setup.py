from setuptools import setup, find_packages

setup(
   name='print_colored_lib',
   version='0.0.3',
   description='This is an opinionated gamedev library that is developed within the Software- and '
               'Game Development lecture at Stuttgart Media University.',
   author='Max Mustermann',
   author_email='rupp@hdm-stuttgart.de',
   packages=find_packages(),
   install_requires=['wheel', 'pygame']
)